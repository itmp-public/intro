let contact = {name: "Bela", age: 34, "your id": "ABC123"};

console.log(contact);
console.log(contact.name);
console.log(contact.age);

contact.age = 35;

console.log(contact);

contact.phone = '+36301234567';
console.log(contact);

let key = 'phone2'; 

console.log(contact[key]);

contact.age = undefined;

console.log(contact);
delete contact.age;
console.log(contact);

console.log(contact['your id']);

let ob1 = {name: "Bela", age: 12};
let ob2 = {name: "Bela", age: 12};

console.log(ob1 === ob2); // a referencia nem egyenlő: nem ugyanarra az objektumra mutat

ob2 = ob1; //ob2 referencia átállítása az ob1 által mutatott objektumra

console.log(ob1 === ob2); // most már ob1 referencia szerint egyenlő ob2-vel.