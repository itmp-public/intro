let names = ["Peter", "Adam", "Zoli"];
console.log(names[0]);
console.log(names[2]);

names[1] = 1;
names[2] = {name: "Bela", age: 34};

console.log(names);
console.log(typeof names[0]);
console.log(typeof names[1]);
console.log(typeof names[2]);
console.log(typeof names);
console.log(names instanceof Array);

let numbers = new Array();
console.log(numbers);
numbers[0] = 10;
numbers[1] = 20;
numbers.push(30, 40);
numbers.unshift(0);
numbers.length = 10;
numbers.push(110);

console.log(numbers);
console.log(numbers.length);

numbers.length = 5;
console.log(numbers.pop());
console.log(numbers.shift());
console.log(numbers[0]);

console.log(numbers);
console.log(numbers.length);

let arr = [];
arr['cica'] = 'hello';
console.log(arr.cica);
arr[-1] = 'hi';


arr['10'] = 'szia';
console.log(arr);
console.log(arr.length);



let arr2 = [1,2,3,4];
arr2[-1]= "alma";

arr2["2"]='aa';

console.log("=====");

for (let item in arr2) {
    console.log("'" + item + "'");
    console.log(typeof item);
}

console.log(arr);


