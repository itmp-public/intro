# JavaScript alapok - mintapéldák

## Telepítések
A böngészők az alábbi linkekről telepíthetőek:

* [Google Chrome](https://www.google.com/chrome/)
* [Mozilla Firefox](https://www.mozilla.org/hu/firefox/new/)

Node.js a [nodejs.org](https://nodejs.org) oldalról az lts verzió kiválasztásával tölthető le. Majd a letöltött telepítővel installálható.

Fejlesztői környezetek:
- [Atom](https://atom.io/)
- [VS Code](https://code.visualstudio.com/) a tanfolyam alatt ezt használjuk

Ajánlott VS Code plugin:
- [Node Exec](https://marketplace.visualstudio.com/items?itemName=miramac.vscode-exec-node)

## Futtatás
Kód futtatása: 
`node basics.js`
paranccsal.

Ha a *Node Exec* plugin telepítve van akkor elég az `F8` billentyűt megnyomni

## További információk:
* [Operátorok precedenciája](https://developer.mozilla.org/hu/docs/Web/JavaScript/Reference/Operators/Operator_Precedence#Table)
* [Stringekről bővebben](https://developer.mozilla.org/hu/docs/Web/JavaScript/Reference/Global_Objects/String)
* [Egyenlőség összehasonlítás](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness)