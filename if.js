let a = 11;

if (a > 20) {
    console.log("A > 20");    
} else if (a > 10) {
    console.log("20 >= A > 10");    
} else {
    console.log("kisebb, vagy egyenlo, mint 10");    
}

// if (a > 10) {
//     if (a > 20) {
//         console.log("A > 20");
//     } else {
//         console.log("20 >= A > 10");
//     }
// } else {
//     console.log("kisebb, vagy egyenlo, mint 10");
// }

let felnott = false;

let message = felnott ? "Felnott" : "Gyerek";

console.log("message: " + message);

let age = 23;

switch (age) {
    case 1: console.log("ONE");
    break;
    case 2: console.log("TWO");
    break;
    default: console.log("DEFAULT");
    break;
    case 3: console.log("THREE");
    break;
}
console.log("VEGE");
