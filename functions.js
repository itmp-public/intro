function calculateBoxArea(width, height){
    height = height || 0;
    width = width || 0;
    console.log(`width: ${width}, height: ${height}`)
    return width * height;
}

function calculateBoxAreaES6(width = 0, height = 0){
    console.log(`width: ${width}, height: ${height}`)
    return {
        width: width,
        height: height,
        area: width * height
    };
};

let calculateBoxAreaReference = function(width, height){
    height = height || 0;
    width = width || 0;
    return width * height;
}

let alma = function(){
    console.log('Ez az alma függvény');
    return function() {console.log('alma')};
}

let calculateBoxAreaArrow = (width, height) => {
    console.log('Ez egy arrow function');
    return width * height;
}

let calculateBoxAreaShortArrow = (width, height) => width * height;
let myLog = message => `myMessage: ${message}`;

console.log(calculateBoxArea(8,3));
console.log(calculateBoxAreaES6(11, 10));
console.log(calculateBoxAreaES6(undefined, 8));
console.log(calculateBoxAreaReference(3,4));
console.log(calculateBoxAreaArrow(3,4));

console.log(myLog('Hello world!'));

let box = {
    width: 12,
    height: 6,
    getBoxSizes: function () {
        console.log(`A doboz méretei: ${this.width}, ${this.height}`);
    },
    getArea(){
        console.log('A doboz területe');
        return this.width * this.height;
    }
}

box.getBoxSizes();
box['get' + 'BoxSizes']();
console.log(box.getArea());

function getMonthName(mo) {
    mo = mo - 1;
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    if (months[mo]) {
      return months[mo];
    } else {
      throw new Error('Nincs ilyen');
    }
  }
let month;
try {
    month = getMonthName(13);
}
catch(e){
    month = 'nem található'
    console.log(e);
}
finally {
    console.log('Sikeres Hibakezelés!');
}

let almaFunc = alma();
almaFunc();

let name = 'Adam';
function logName(){
    // let name = 'Peter';
    function logAnotherName(){
        // let name = 'Geza';
        console.log(name);
    }
    logAnotherName();
}
logName();

function logNameClosure(){
    let name = 'Peter';
    function logAnotherName(){
        console.log('Another name: ' + name);
    }
    return logAnotherName;
}
let a = logNameClosure()
console.log(a);
a();